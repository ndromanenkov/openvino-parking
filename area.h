#pragma once
#include <opencv2/opencv.hpp>
#include <vector>

struct mArea
{
	mArea(std::vector<cv::Point>& vec);
	
	cv::Point min;
	cv::Point max;
	
	unsigned height;
	unsigned width;
	
	bool operator < (const mArea& a) const;
	bool operator == (const mArea& a) const;
};
