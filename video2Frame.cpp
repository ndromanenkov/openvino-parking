void video2Frame(char* filename)
{

        printf("[i] file: %s\n", filename);

        // окно для отображения картинки
        cvNamedWindow("original",CV_WINDOW_AUTOSIZE);

        // получаем информацию о видео-файле
        CvCapture* capture = cvCreateFileCapture( filename );

        while(1){
                // получаем следующий кадр
                frame = cvQueryFrame( capture ); 
                if( !frame ) {
                        break;
                }

                // здесь можно вставить
                // процедуру обработки

                // показываем кадр
                cvShowImage( "original", frame );

                char c = cvWaitKey(33);
                if (c == 27) { // если нажата ESC - выходим
                        break;
                }
        }

        // освобождаем ресурсы
        cvReleaseCapture( &capture );
        // удаляем окно
        cvDestroyWindow("original");
}